package com.example.android.tablayoutandviewpager;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

/**
 * Swiping feature with tab layout and viewpager
 */
public class MainActivity extends AppCompatActivity {

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final    SampleFragmentPagerAdapter adapter =  new SampleFragmentPagerAdapter(getSupportFragmentManager());

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);

        // Getting ViewPager and setting up an adapter in it
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
